const fs = require('fs');

console.log('dir：', __dirname)
console.log('file：', __filename)

// flags 参数可以是以下值：
// Flag	描述
// r	以读取模式打开文件。如果文件不存在抛出异常。
// r+	以读写模式打开文件。如果文件不存在抛出异常。
// rs	以同步的方式读取文件。
// rs+	以同步的方式读取和写入文件。
// w	以写入模式打开文件，如果文件不存在则创建。
// wx	类似 'w'，但是如果文件路径存在，则文件写入失败。
// w+	以读写模式打开文件，如果文件不存在则创建。
// wx+	类似 'w+'， 但是如果文件路径存在，则文件读写失败。
// a	以追加模式打开文件，如果文件不存在则创建。
// ax	类似 'a'， 但是如果文件路径存在，则文件追加失败。
// a+	以读取追加模式打开文件，如果文件不存在则创建。
// ax+	类似 'a+'， 但是如果文件路径存在，则文件读取追加失败。

// 异步读取
function readFile(path){
    fs.readFile(path, function(err, data){
        if(err){
            return console.error(err);
        }

        res = data.toString();
        console.log('1',res);
    })
}

// readFile('./test.txt');

const path = __dirname+'/test.txt';

// 同步读取
let res = fs.readFileSync(path)
console.log('同步读取test.txt', res.toString())

// 写入文件
content = '\n这里是追加内容';
fs.writeFile(path, content,{'flag': 'a'}, function(err){
    if(err){
        return console.error(err);
    }
    console.log("数据写入成功！");
    console.log("--------我是分割线-------------")
    console.log("读取写入的数据！");
    fs.readFile(path, function (err, data) {
        if (err) {
           return console.error(err);
        }
        console.log("异步读取文件数据: " + data.toString());
     });
});

