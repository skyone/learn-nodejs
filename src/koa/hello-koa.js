const Koa = require('koa');
const fs = require('fs');
const route = require('koa-route');

// 创建一个Koa兑现表示web app本身
const app = new Koa();

// app.use(async (ctx, next) => {
//     await next();
//     ctx.response.type = 'text/html';
//     ctx.response.body = '<h1>hello koa!</h1>';
// });

// 中间件
// logger函数就叫做"中间件"（middleware），因为它处在 HTTP Request 和 HTTP Response 中间，用来实现某种中间功能。app.use()用来加载中间件。
// 每个中间件默认接受两个参数，第一个参数是 Context 对象，第二个参数是next函数。只要调用next函数，就可以把执行权转交给下一个中间件。
const logger = (ctx, next) => {
    console.log(`${Date.now()} ${ctx.request.method} ${ctx.request.url}`);
    next();
  }
app.use(logger);

const main = ctx => {
    if(ctx.request.accepts('json')){
        ctx.response.body={data: 'hello koa'}
        ctx.response.type = 'json';
    }else if(ctx.request.accepts('html')){
        ctx.response.type='html';
        ctx.response.body = '<p>hello koa!</p>';
    }else{
        ctx.response.type='text';
        ctx.response.body = "Hello Koa!";
    }
};

// 读取文件/模板
const txt = ctx => {
    let path = __dirname+'\\text.txt';
    console.log(path);
    ctx.response.type='html';
    ctx.response.body = fs.createReadStream(path);
};

// 路由
const routeTest = ctx => {
    if(ctx.request.path !== '/'){
        ctx.response.type = 'html';
        ctx.response.body = '<a href="/">Index Page</a>';
    }else{
        ctx.response.body = 'Hello World';
    }
}

// koa-route 模块
const about = ctx => {
    ctx.response.type='html';
    ctx.response.body = '<a href="/">Index Page</a>';
};

const koaRoute = ctx => {
    ctx.response.body='hello Koa!';
};

app.use(route.get('/', koaRoute));
app.use(route.get('/about', about));

// koa-static
const path = require('path');
const serve = require('koa-static');

//  http://localhost:3000/text.txt
const staticRes = serve(path.join(__dirname));
app.use(staticRes);

// 重定向
const redirect = ctx => {
    // console.log(`${Date.now()} ${ctx.request.method} ${ctx.request.url}`);
    ctx.response.redirect('/about');
    ctx.response.body = '<a href="/">Index Page</a>';
};
app.use(route.get('/redirect', redirect));

app.listen(3000);
console.log('app started at port 3000');