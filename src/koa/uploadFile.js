const KoaBody = require('koa-body');
const Koa = require('koa');
const os = require('os');
const path = require('path');
const fs = require('fs');

const app = new Koa();

// 异步
const main = async function(ctx){
    const tmdir = os.tmdir;
    const filePaths = [];
    const files = ctx.request.body.files || {};

    for (let key in files){
        const file = files[key];
        const filepath = path.join(tmdir, file.name);
        const reader = fs.createReadStream(file.path);
        const writer = fs.createWriteStream(filepath);
        reader.pipe(writer);
        filePaths.push(filepath);
    }

    ctx.body = filepaths;
};

app.use(KoaBody({ multipart: true }));
app.use(main);
app.listen(3000);
console.log('Server started on 3000');