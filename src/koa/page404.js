const Koa = require('koa');
const app = new Koa();

// 04错误
const page404 = ctx => {
    ctx.response.status = 404;
    ctx.response.body = 'page not found';
}

app.use(page404);
app.listen(3000);
console.log('app started at port 3000');