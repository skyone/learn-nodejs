const MongodbClient = require('mongodb').mongodbClient;
const url = 'mongodb://127.0.0.1:27017/node';

MongodbClient.connect(url, function(err, db){
    if(err) throw err;
    console.log("数据库已创建");
    db.close();
});